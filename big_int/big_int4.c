/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_int4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/27 16:13:17 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/27 16:32:30 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"

uint64_t		div_64(uint64_t val_1, uint64_t val_2)
{
	int				i;
	uint64_t		remain;
	uint64_t		quotient;
	uint64_t		one;

	i = 64;
	remain = 0;
	quotient = 0;
	one = 1;
	while (i > -1)
	{
		remain = remain << 1;
		if (val_1 & (one << i))
			remain += 1;
		if (remain >= val_2)
		{
			remain = remain - val_2;
			quotient += (one << i);
		}
		i--;
	}
	return (quotient);
}

void			div_init(t_big_int *remain, t_big_int *quotient, t_big_int *one,
							uint64_t *llu)
{
	*remain = big_zero_init();
	*quotient = big_zero_init();
	quotient->size = 64;
	*one = big_one_init();
	*llu = 1;
}

t_big_int		div_big_int(t_big_int nb1, t_big_int nb2)
{
	int			j;
	t_big_int	remain;
	t_big_int	quotient;
	t_big_int	one;
	uint64_t	llu;

	div_init(&remain, &quotient, &one, &llu);
	while (--nb1.size > -1)
	{
		j = 64;
		while (--j > -1)
		{
			remain = shift_left(remain);
			if (nb1.val[nb1.size] & (llu << j))
				remain = add_big_int(remain, one);
			if (comp_big_int(remain, nb2) >= 0)
			{
				remain = sub_big_int(remain, nb2);
				quotient.val[nb1.size] += (llu << j);
			}
		}
	}
	while (quotient.val[quotient.size - 1] == 0 && quotient.size > 0)
		quotient.size -= 1;
	return (quotient);
}

t_big_int		mod_big_int(t_big_int nb1, t_big_int nb2)
{
	int			j;
	t_big_int	remain;
	t_big_int	quotient;
	t_big_int	one;
	uint64_t	llu;

	div_init(&remain, &quotient, &one, &llu);
	while (--nb1.size > -1)
	{
		j = 64;
		while (--j > -1)
		{
			remain = shift_left(remain);
			if (nb1.val[nb1.size] & (llu << j))
				remain = add_big_int(remain, one);
			if (comp_big_int(remain, nb2) >= 0)
			{
				remain = sub_big_int(remain, nb2);
				quotient.val[nb1.size] += (llu << j);
			}
		}
	}
	while (++nb1.size < 64 && quotient.val[nb1.size + 1] != 0)
		quotient.size += 1;
	return (remain);
}
