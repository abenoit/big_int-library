/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_int.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/24 11:03:04 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/27 17:02:49 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"

int					comp_big_int(t_big_int nb1, t_big_int nb2)
{
	int i;

	if (nb1.size == nb2.size)
	{
		i = nb1.size - 1;
		while (i > -1)
		{
			if (nb1.val[i] == nb2.val[i])
				i--;
			else
				return ((nb1.val[i] > nb2.val[i]) ? 1 : -1);
		}
		return (0);
	}
	else
		return ((nb1.size > nb2.size) ? 1 : -1);
}

uint16_t			add_8(uint8_t val_1, uint8_t val_2, uint64_t *carry)
{
	uint16_t		tmp;
	int				i;
	int				res;

	i = -1;
	res = 0;
	while (++i < 8)
	{
		tmp = 0;
		if (val_1 & (1 << i) && val_2 & (1 << i))
			*carry += (1 << (i + 1));
		else if (val_1 & (1 << i) || val_2 & (1 << i))
			tmp += (1 << i);
		if (tmp & (1 << i) && *carry & (1 << i))
			*carry += (1 << (i + 1)) - (1 << i);
		else if (tmp & (1 << i) || *carry & (1 << i))
		{
			res += (1 << i);
			*carry & (1 << i) ? (*carry -= (1 << i)) : (*carry = *carry);
		}
	}
	return (res);
}

uint64_t			add_64(uint64_t val_1, uint64_t val_2, uint64_t *carry)
{
	uint8_t			*tmp1;
	uint8_t			*tmp2;
	uint64_t		res;
	uint64_t		tmp;
	int				i;

	i = 8;
	res = 0;
	tmp = 0;
	tmp1 = (uint8_t*)&val_1;
	tmp2 = (uint8_t*)&val_2;
	while (i > 0)
	{
		tmp = (uint64_t)add_8(*tmp1, *tmp2, carry);
		res += tmp << (8 * (8 - i));
		if (*carry > 0)
			*carry = 1;
		i--;
		tmp1++;
		tmp2++;
	}
	return (res);
}

t_big_int			add_big_int(t_big_int nb1, t_big_int nb2)
{
	uint64_t	carry;
	int			i;
	t_big_int	result;

	i = 0;
	carry = 0;
	result = big_zero_init();
	result.size = ((comp_big_int(nb1, nb2) > 0) ? nb1.size : nb2.size);
	while (i < result.size)
	{
		result.val[i] = add_64(nb1.val[i], nb2.val[i], &carry);
		i++;
	}
	if (carry != 0 && result.size < 63)
	{
		result.size += 1;
		result.val[i] = carry;
	}
	return (result);
}
