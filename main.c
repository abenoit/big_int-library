/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/27 16:18:31 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/23 17:52:54 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"
#include "ft_printbits.h"
#include "print_floats.h"

int		main(void)
{
	float			a = -0.7864;
	double			b = -0.5;
	long double 	c = 0.08964;
	t_float_struct	ret;
	t_big_float		lol;

	ret = dbl_extract(b);
	print_float_struct(ret);
//	lol = parse_big_double(ret);
//	print_float(lol);
	print_sci(lol);
	printf("%e\n", b);
}
