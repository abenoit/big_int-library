/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_floats.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/28 17:43:41 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/23 09:42:19 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_floats.h"
#include "big_int.h"
#include "ft_printbits.h"

void			print_int(uint64_t nb)
{
	char		buff;

	if (nb < 10)
	{
		buff = nb + 48;
		write(1, &buff, 1);
	}
	else
	{
		print_int(nb / 10);
		print_int(nb % 10);
	}
}

void			print_big_int(t_big_int nbr)
{
	t_big_int	zero;
	t_big_int	ten;
	char		buff;

	zero = big_zero_init();
	ten = big_zero_init();
	ten.val[0] = 10;
	if (comp_big_int(nbr, ten) < 0)
	{
		buff = (int)nbr.val[0] + 48;
		write(1, &buff, 1);
	}
	else
	{
		print_big_int(div_big_int(nbr, ten));
		print_big_int(mod_big_int(nbr, ten));
	}
}

void			print_float_struct(t_float_struct nbr)
{
	char		temp;

	temp = nbr.sign + 48;
	write(1, &temp, 1);
	write(1, "\n", 1);
	ft_printbits(&nbr.exp, 4);
	write(1, "\n", 1);
	ft_printbits(&nbr.mant, 8);
	write(1, "\n", 1);
	temp = nbr.sign + 48;
	write(1, &temp, 1);
	write(1, "\n", 1);
	print_int(nbr.exp);
	write(1, "\n", 1);
	print_int(nbr.mant);
	write(1, "\n", 1);
}

t_big_int			print_big_frac(t_big_float nbr)
{
	t_big_int	one;
	t_big_int	ten;
	t_big_int	temp;

	one = big_one_init();
	ten = big_ten_init();
	temp = big_zero_init();
	nbr.max = div_big_int(nbr.max, ten);
	while (comp_big_int(nbr.high, nbr.max) < 0)
	{
		write(1, "0", 1);
		nbr.max = div_big_int(nbr.max, ten);
	}
	temp = test_margin(&nbr, &temp);
	return (temp);
}

t_big_int			print_big_frac_sized(t_big_float nbr, int n)
{
	t_big_int	one;
	t_big_int	ten;
	t_big_int	temp;

	one = big_one_init();
	ten = big_ten_init();
	temp = big_zero_init();
	mod_margins_sized(&nbr, n);
	nbr.max = div_big_int(nbr.max, ten);
	while (comp_big_int(nbr.high, nbr.max) < 0)
	{
		write(1, "0", 1);
		nbr.max = div_big_int(nbr.max, ten);
	}
	temp = test_margin(&nbr, &temp);
	return (temp);
}

void			print_float(t_big_float nbr)
{
	t_big_int	two;
	t_big_int	ten;
	t_big_int	frac;

	ten = big_ten_init();
	if (nbr.sign == 1)
		write(1, "-", 1);
	print_big_int(nbr.ent);
	write(1, ".", 1);
	frac = print_big_frac(nbr);
	print_big_int(frac);
	write(0, "\n", 1);
}

void			print_float_sized(t_big_float nbr, int n)
{
	t_big_int	two;
	t_big_int	ten;
	t_big_int	frac;

	ten = big_ten_init();
	if (nbr.sign == 1)
		write(1, "-", 1);
	print_big_int(nbr.ent);
	write(1, ".", 1);
	frac = print_big_frac_sized(nbr, n);
	print_big_int(frac);
	write(0, "\n", 1);
}
