/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_floats.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/28 12:03:03 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/25 10:44:31 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINT_FLOATS_H
# define PRINT_FLOATS_H

# include "big_int.h"

# define LOG2_10 3.32192809489
# define MAX_DIGIT_FLOAT 10
# define MAX_DIGIT_DOUBLE 17
# define MAX_DIGIT_EXTENDED 20
# define MAX_DIGIT_QUAD 36

typedef enum		e_float_format
{
	FLOAT,
	DOUBLE,
	LONG_DOUBLE
}					t_float_format;

typedef struct		s_big_float
{
	t_big_int	max;
	t_big_int	ent;
	t_big_int	frac;
	t_big_int	high;
	t_big_int	low;
	t_big_int	high_margin;
	t_big_int	low_margin;
	int			exp;
	int			max_exp;
	char		sign;
	char		*int_str;
	char		*frac_str;
}					t_big_float;

typedef struct		s_float_struct
{
	t_float_format	format;
	uint64_t		mant;
	uint32_t		exp;
	char			sign;
}					t_float_struct;

typedef union		u_float
{
	float	flt;
	char	cast[4];
}					t_float;

typedef union		u_double
{
	double		dbl;
	char		cast[8];
}					t_double;

typedef union		u_long_double
{
	long double		long_dbl;
	char			cast[16];
}					t_long_double;

void				print_bits_fl(float nb);
void				print_bits_dbl(double nb);
void				print_bits_long_dbl(long double nb);

t_float_struct		flt_extract(float nb);
t_float_struct		dbl_extract(double nb);
t_float_struct		long_dbl_extract(long double nb);

void				print_int(uint64_t nbr);
void				print_big_int(t_big_int nbr);
void				print_float_struct(t_float_struct nbr);
t_big_int			print_big_frac(t_big_float nbr);
t_big_int			print_big_frac_sized(t_big_float nbr, int n);
void				print_float(t_big_float nbr);
void				print_float_sized(t_big_float nbr, int n);

void				init_big_double(t_float_struct nbr, t_big_float *ret);
void				split_big_double(t_float_struct nbr, t_big_float *ret);
t_big_float			parse_big_double(t_float_struct nbr);

void				init_big_float(t_float_struct nbr, t_big_float *ret);
void				split_big_float(t_float_struct nbr, t_big_float *ret);
t_big_float			parse_big_float(t_float_struct nbr);

void				init_big_long_double(t_float_struct nbr, t_big_float *ret);
void				split_big_long_double(t_float_struct nbr, t_big_float *ret);
t_big_float			parse_big_long_double(t_float_struct nbr);

void				mod_margins_sized(t_big_float *nbr, int n);
t_big_int			test_margin(t_big_float *nbr, t_big_int *temp);
t_big_int			margin(t_big_float *nbr, t_big_int *temp);

void				print_sci(t_big_float nbr);

#endif
