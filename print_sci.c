/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_sci.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/23 17:21:12 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/25 10:41:05 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"
#include "print_floats.h"

void		print_sci(t_big_float nbr)
{
	t_big_int	zero;
	t_big_int	ten;
	int			k;

	zero = big_zero_init();
	if (comp_big_int(nbr.ent, zero) != 0)
	{
		while (comp_big_int(div_big_int(nbr.ent, pow_big_int(ten, k)), zero) != 0)
			k++;
		print_big_int(nbr.ent);
	}

}
