/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floats.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 19:14:47 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/15 21:22:28 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"
#include "ft_printbits.h"

t_float_struct	parse_floats(double nbr)
{
	int					i;
	int					j;
	uint64_t			tmp;
	t_double			cast_nb;
	t_float_struct		float_nb;

	i = -1;
	float_nb.mant = 0;
	float_nb.exp = 0;
	float_nb.sign = 0;
	cast_nb.dbl = nbr;
	ft_printbits(&cast_nb.dbl, 8);
	printf("\n");
	while (++i < 8)
	{
		j = -1;
		tmp = 0;
		while (++j < 8)
		{
			if (((i == 6 && j < 4) || (i < 6)) &&  cast_nb.cast[i] & (1 << j))
				tmp += (1 << j);
			if (i > 5)
			{
				if (i == 7 && j == 7)
					if (cast_nb.cast[i] & (1 << j))
							float_nb.sign = 1;
				if ((i == 7 && j != 7) || (i == 6 && j > 3))
					if (cast_nb.cast[i] & (1 << j))
							float_nb.exp += (1 << (j + (8 * i)));
			}
		}
		float_nb.mant += tmp << (8 * i);
	}
	tmp = 1;
//	float_nb.mant += tmp << 52;
//	ft_printbits(&float_nb.mant, 8);
//	printf("\n%llu\n", float_nb.mant);
	float_nb.exp = float_nb.exp >> 20;
//	ft_printbits(&float_nb.exp, 4);
//	printf("\n%u\n", float_nb.exp);
//	ft_printbits(&float_nb.sign, 1);
//	printf("\n%c\n", float_nb.sign + 48);
	return (float_nb);
}

/*
t_float	parse_floats(double nbr)
{
	int			i;
	int			j;
	uint64_t		tmp;
	t_double	cast_nb;
	t_float_struct		float_nb;

	i = 0;
	float_nb.mant = 0;
	cast_nb.dbl = nbr;
	ft_printbits(&cast_nb.dbl, 8);
	printf("\n");
	while (i < 8)
	{
		j = -1;
		tmp = 0;
		while (++j < 8)
		{
			if (i < 2)
			{
				if (i == 0 && j == 0)
					if (cast_nb.cast[i] & (1 << j))
							float_nb.sign = 1;
				if ((i == 0 && j > 0) || i == 1)
					if (cast_nb.cast[i] & (1 << j))
							float_nb.exp = 1;
			}
			if (((i == 1 && j > 3) || (i > 1)) &&  cast_nb.cast[i] & (1 << j))
				tmp += (1 << j);
		}
		float_nb.mant += tmp << (8 * i);
		i++;
	}
	ft_printbits(&float_nb.mant, 8);
	printf("\n");
	ft_printbits(&float_nb.exp, 4);
	printf("\n");
	ft_printbits(&float_nb.sign, 1);
	printf("\n");
}*/
