/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floats_extract.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/28 13:30:10 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/15 21:37:16 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printbits.h"
#include "print_floats.h"

t_float_struct		flt_extract(float nb)
{
	t_float			nbr;
	t_float_struct	ret;
	int				i;
	uint8_t			u;

	nbr.flt = nb;
	ret.exp = 0;
	ret.mant = 0;
	ret.format = FLOAT;
	i = 31;
	u = 1;
	while (i > -1)
	{
		if (nbr.cast[i / 8] & (u << (i % 8)))
		{
			if (i == 31)
				ret.sign = 1;
			else if (i < 31 && i > 22)
				ret.exp += ((u << i) >> 23);
			else
				ret.mant += (u << i);
		}
		i--;
	}
	return (ret);
}

t_float_struct		dbl_extract(double nb)
{
	t_double		nbr;
	t_float_struct	ret;
	int				i;
	uint64_t		u;

	nbr.dbl = nb;
	ret.exp = 0;
	ret.mant = 0;
	ret.format = DOUBLE;
	i = 63;
	u = 1;
	while (i > -1)
	{
		if (nbr.cast[i / 8] & (u << (i % 8)))
		{
			if (i == 63)
				ret.sign = 1;
			else if (i < 63 && i > 51)
				ret.exp += ((u << (i % 32)) >> 20);
			else
				ret.mant += (u << i);
		}
		i--;
	}
	return (ret);
}

t_float_struct		long_dbl_extract(long double nb)
{
	t_long_double		nbr;
	t_float_struct		ret;
	int					i;
	uint64_t			u;

	nbr.long_dbl = nb;
	ret.exp = 0;
	ret.mant = 0;
	ret.format = LONG_DOUBLE;
	i = 79;
	u = 1;
	while (i > -1)
	{
		if (nbr.cast[i / 8] & (u << (i % 8)))
		{
			if (i == 79)
				ret.sign = 1;
			else if (i < 79 && i > 63)
				ret.exp += ((u << (i % 32)));
			else
				ret.mant += (u << i);
		}
		i--;
	}
	return (ret);
}
