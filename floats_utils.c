/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floats_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/28 12:19:44 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/29 11:07:11 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_floats.h"

void		print_bits_fl(float nb)
{
	t_float	nbr;
	int		i;
	uint8_t	u;

	nbr.flt = nb;
	i = 31;
	u = 1;
	while (i > -1)
	{
		if (nbr.cast[i / 8] & (u << (i % 8)))
			write(1, "1", 1);
		else
			write(1, "0", 1);
		i--;
	}
	write(1, "\n", 1);
}

void		print_bits_dbl(double nb)
{
	t_double	nbr;
	int			i;
	uint8_t		u;

	nbr.dbl = nb;
	i = 63;
	u = 1;
	while (i > -1)
	{
		if (nbr.cast[i / 8] & (u << (i % 8)))
			write(1, "1", 1);
		else
			write(1, "0", 1);
		i--;
	}
	write(1, "\n", 1);
}

void		print_bits_long_dbl(long double nb)
{
	t_long_double	nbr;
	int				i;
	uint8_t			u;

	nbr.long_dbl = nb;
	i = 79;
	u = 1;
	while (i > -1)
	{
		if (nbr.cast[i / 8] & (u << (i % 8)))
			write(1, "1", 1);
		else
			write(1, "0", 1);
		i--;
	}
	write(1, "\n", 1);
}
