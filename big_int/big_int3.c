/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_int3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/24 19:12:49 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/23 16:56:36 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"

t_big_int		process_mult(t_big_int biggy, t_big_int small)
{
	t_big_int		tmp;
	uint64_t		k;
	int				i;
	int				j;

	i = 0;
	k = 1;
	tmp = big_zero_init();
	while (i < small.size)
	{
		j = -1;
		while (++j < 64)
		{
			if (small.val[i] & (k << j))
				tmp = add_big_int(tmp, biggy);
			biggy = shift_left(biggy);
		}
		i++;
	}
	return (tmp);
}

t_big_int		mult_big_int(t_big_int nb1, t_big_int nb2)
{
	t_big_int		biggy;
	t_big_int		small;
	t_big_int		result;

	biggy = ((comp_big_int(nb1, nb2) >= 0) ? nb1 : nb2);
	small = ((comp_big_int(nb1, nb2) < 0) ? nb1 : nb2);
	result = big_zero_init();
	result.size = biggy.size + small.size;
	result = process_mult(biggy, small);
	return (result);
}

t_big_int		pow_big_int(t_big_int nb, int pow)
{
	t_big_int one;
	t_big_int zero;

	one = big_one_init();
	zero = big_zero_init();
	if (pow == 1)
		return (nb);
	else if (pow < 0)
		return (zero);
	else if (pow == 0)
		return (one);
	else
	{
		nb = mult_big_int(nb, pow_big_int(nb, pow - 1));
	}
	return (nb);
}

t_big_int		shift_right(t_big_int nb)
{
	int			i;
	char		carry[64];

	i = -1;
	while (++i < 64)
		carry[i] = 0;
	i = nb.size - 1;
	while (i > -1)
	{
		if (nb.val[i] & 1)
		{
			if (i != 0)
				carry[i - 1] = 1;
		}
		nb.val[i] = nb.val[i] >> 1;
		if (carry[i] != 0)
			nb.val[i] += (uint64_t)LONG_MIN;
		i--;
	}
	while (nb.val[nb.size - 1] == 0 && nb.size != 1)
		nb.size--;
	return (nb);
}

t_big_int		shift_left(t_big_int nb)
{
	int			i;
	char		carry[64];
	uint64_t	lol;

	lol = 1;
	i = 64;
	while (--i > -1)
		carry[i] = 0;
	while (++i < nb.size)
	{
		if (nb.val[i] & (lol << 63))
		{
			if (i != 64)
				carry[i + 1] = 1;
		}
		nb.val[i] = nb.val[i] << 1;
		if (carry[i] != 0)
			nb.val[i] += lol;
	}
	if (nb.size < 64 && carry[nb.size] != 0)
	{
		nb.val[nb.size] += 1;
		nb.size += 1;
	}
	return (nb);
}
