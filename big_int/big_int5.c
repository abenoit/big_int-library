/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_int5.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/27 16:07:28 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/28 12:11:36 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"

t_big_int			big_zero_init(void)
{
	int			i;
	t_big_int	nb;

	i = 0;
	while (i < 64)
	{
		nb.val[i] = 0;
		i++;
	}
	nb.size = 1;
	return (nb);
}

t_big_int			big_one_init(void)
{
	int			i;
	t_big_int	nb;

	i = -1;
	while (++i < 64)
		nb.val[i] = 0;
	nb.val[0] = 1;
	nb.size = 1;
	return (nb);
}

t_big_int			big_two_init(void)
{
	int			i;
	t_big_int	nb;

	i = -1;
	while (++i < 64)
		nb.val[i] = 0;
	nb.val[0] = 2;
	nb.size = 1;
	return (nb);
}

t_big_int			big_ten_init(void)
{
	int			i;
	t_big_int	nb;

	i = -1;
	while (++i < 64)
		nb.val[i] = 0;
	nb.val[0] = 10;
	nb.size = 1;
	return (nb);
}
