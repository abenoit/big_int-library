/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_margins.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/01 18:01:07 by abenoit           #+#    #+#             */
/*   Updated: 2020/06/23 16:59:49 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print_floats.h"
#include "big_int.h"
#include "ft_printbits.h"

void			mod_margins_sized(t_big_float *nbr, int n)
{
	t_big_int	two;
	t_big_int	ten;

	two = big_two_init();
	ten = big_ten_init();
	nbr->high = add_big_int(nbr->frac, div_big_int(pow_big_int(ten, (nbr->max_exp - n)), two));
	nbr->low = sub_big_int(nbr->frac, div_big_int(pow_big_int(ten, (nbr->max_exp - n)), two));
}

t_big_int		test_margin(t_big_float *nbr, t_big_int *temp)
{
	t_big_int	plop;
	t_big_int	lol;
	t_big_int	zero;
	t_big_int	one;
	t_big_int	ten;
	t_big_int	digit;

	zero = big_zero_init();
	one = big_one_init();
	ten = big_ten_init();
	if (comp_big_int(nbr->frac, zero) == 0)
		return (*temp);
	digit = div_big_int(nbr->frac, nbr->max);
	nbr->frac = mod_big_int(nbr->frac, nbr->max);
	*temp = mult_big_int(*temp, ten);
	plop = mult_big_int(add_big_int(*temp, digit), nbr->max);
	lol = mult_big_int(add_big_int(*temp, add_big_int(digit, one)), nbr->max);
	*temp = add_big_int(*temp, digit);
	nbr->max = div_big_int(nbr->max, ten);
	if ((comp_big_int(nbr->high, lol) >= 0) && (comp_big_int(nbr->low, plop) <= 0))
	{
		if (comp_big_int(sub_big_int(*temp, nbr->low), sub_big_int(nbr->high, *temp)) < 0)
			return (*temp);
		else
			return(add_big_int(*temp, one));
	}
	else if ((comp_big_int(nbr->high, lol) > 0) && !(comp_big_int(nbr->low, plop) < 0))
		return(add_big_int(*temp, one));
	else if (!(comp_big_int(nbr->high, lol) > 0) && (comp_big_int(nbr->low, plop) < 0))
		return (*temp);
	else
		return (test_margin(nbr, temp));
}

t_big_int		margin(t_big_float *nbr, t_big_int *temp)
{
	t_big_int	plop;
	t_big_int	lol;
	t_big_int	zero;
	t_big_int	one;
	t_big_int	ten;
	t_big_int	digit;

	zero = big_zero_init();
	one = big_one_init();
	ten = big_ten_init();
	digit = div_big_int(nbr->frac, nbr->max);
	nbr->frac = mod_big_int(nbr->frac, nbr->max);
	*temp = mult_big_int(*temp, ten);
	plop = mult_big_int(add_big_int(*temp, digit), nbr->max);
	lol = mult_big_int(add_big_int(*temp, add_big_int(digit, one)), nbr->max);
	*temp = add_big_int(*temp, digit);
	nbr->max = div_big_int(nbr->max, ten);
	if (((comp_big_int(nbr->high, lol) > 0) && (comp_big_int(nbr->low, plop) < 0)) || comp_big_int(nbr->frac, zero) == 0)
	{
		if (comp_big_int(sub_big_int(*temp, nbr->low), sub_big_int(nbr->high, *temp)) < 0)
			return(add_big_int(*temp, one));
		else
			return (*temp);
	}
	else if (comp_big_int(nbr->high, lol) > 0)
		return(add_big_int(*temp, one));
	else if (comp_big_int(nbr->low, plop) < 0)
		return (*temp);
	else
		return (test_margin(nbr, temp));
}
