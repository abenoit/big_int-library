/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_int.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 16:13:31 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/28 12:16:54 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BIG_INT_H
# define BIG_INT_H

# include <stdint.h>
# include <unistd.h>
# include <stdio.h>
# include <limits.h>

typedef struct		s_int
{
	uint64_t	val[64];
	int			size;
	char		sign;
}					t_big_int;

int					comp_big_int(t_big_int nb1, t_big_int nb2);
t_big_int			add_big_int(t_big_int nb1, t_big_int nb2);

t_big_int			sub_big_int(t_big_int nb1, t_big_int nb2);
t_big_int			shift_right(t_big_int nb);
t_big_int			shift_left(t_big_int nb);

t_big_int			mult_big_int(t_big_int nb1, t_big_int nb2);
t_big_int			pow_big_int(t_big_int nb, int pow);

t_big_int			div_big_int(t_big_int nb1, t_big_int nb2);
t_big_int			mod_big_int(t_big_int nb1, t_big_int nb2);

t_big_int			big_zero_init(void);
t_big_int			big_one_init(void);
t_big_int			big_two_init(void);
t_big_int			big_ten_init(void);

#endif
