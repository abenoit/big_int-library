/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbits.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 16:06:26 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/27 19:24:03 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTBITS_H
# define FT_PRINTBITS_H

# include <stdio.h>
# include <unistd.h>
# include <stdint.h>
# include "big_int.h"

void	ft_printbig(t_big_int nb);
void	ft_printbits(void *ptr, int size);

#endif
