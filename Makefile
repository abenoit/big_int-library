# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/05/27 16:37:50 by abenoit           #+#    #+#              #
#    Updated: 2020/06/23 17:51:36 by abenoit          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = main.c \
		floats_utils.c \
		floats_extract.c \
		print_floats.c \
		split_big_long_double.c \
		split_big_double.c \
		split_big_float.c \
		calc_margins.c \
		print_sci.c

OBJ = $(SRC:.c=.o)

NAME = floats

INC = -I big_int -I ft_printbits

BITS_DIR = ft_printbits

BITS = $(BITS_DIR)/libft_printbits.a

BIG_DIR = big_int

BIG_INT = $(BIG_DIR)/libbig_int.a

CC = clang
RM = rm -f

CFLAGS = -Wall -Werror -Wextra

all: $(BITS) $(BIG_INT) $(NAME)

lol: all
	./$(NAME)

$(BITS):
		@make -C $(BITS_DIR)

$(BIG_INT):
		@make -C $(BIG_DIR)

.c.o:
		$(CC) $(INC) -c $< -o $(<:.c=.o) 

$(NAME): $(OBJ)
		$(CC) -o $(NAME) $(OBJ) $(INC) $(BITS) $(BIG_INT)

clean:
		${RM} ${OBJ}
		@make -C $(BITS_DIR) clean
		@make -C $(BIG_DIR) clean

fclean:		clean
		${RM} ${NAME}
		@make -C $(BITS_DIR) fclean
		@make -C $(BIG_DIR) fclean

re:		fclean all

.PHONY:	clean re fclean
