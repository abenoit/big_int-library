/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbits.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 16:08:10 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/27 16:54:01 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"
#include "ft_printbits.h"

void	ft_printbits(void *ptr, int size)
{
	uint8_t		*data;
	int			count;
	uint8_t		i;
	uint8_t		lu;

	lu = 1;
	count = 0;
	data = (uint8_t*)(ptr) + (size - 1);
	while (count < size)
	{
		i = 0;
		while (i < 8)
		{
			if (*data & (lu << (7 - i)))
				write(0, "1", 1);
			else
				write(0, "0", 1);
			i++;
		}
		data--;
		count++;
	}
}

void	ft_printbig(t_big_int nb)
{
	int			i;

	i = nb.size - 1;
	printf("size = %d\n", nb.size);
	while (i > -1)
	{
		ft_printbits(&nb.val[i], 8);
		write(1, " ", 1);
		i--;
	}
	write(1, "\n", 1);
}
