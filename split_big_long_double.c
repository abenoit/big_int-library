/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_big_long_double.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <abenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 11:44:58 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/29 13:50:01 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"
#include "ft_printbits.h"
#include "print_floats.h"

void				init_big_long_double(t_float_struct nbr, t_big_float *ret)
{
	t_big_int		ten;
	uint64_t		lu;
	int				i;
	int				temp;

	lu = 1;
	i = 0;
	temp = ret->exp;
	ten = big_ten_init();
	ret->max_exp = -1;
	ret->ent = big_zero_init();
	ret->frac = big_zero_init();
	ret->max = big_zero_init();
	while (i < 64 && temp < 0)
	{
		if (nbr.mant & (lu << i))
		{
			ret->max_exp = -(temp);
			break ;
		}
		i++;
		temp++;
	}
	if (ret->max_exp >= 0)
		ret->max = pow_big_int(ten, ret->max_exp);
}

void				split_big_long_double(t_float_struct nbr, t_big_float *ret)
{
	int				i;
	t_big_int		two;
	uint64_t		lu;

	ret->sign = nbr.sign;
	two = big_two_init();
	i = 0;
	lu = 1;
	while (i < 64)
	{
		if (nbr.mant & (lu << i))
		{
			if (ret->exp >= 0)
				ret->ent = add_big_int(ret->ent, pow_big_int(two, ret->exp));
			else if (ret->exp < 0)
				ret->frac = add_big_int(ret->frac,
							div_big_int(ret->max, pow_big_int(two, -ret->exp)));
		}
		ret->exp++;
		i++;
	}
}

t_big_float			parse_big_long_double(t_float_struct nbr)
{
	t_big_float		ret;
	uint64_t		lu;

	lu = 1;
	if (nbr.exp != 0)
		ret.exp = (int)nbr.exp - 16383 - 63;
	else
		ret.exp = 16382 - 63;
	init_big_long_double(nbr, &ret);
	split_big_long_double(nbr, &ret);
	return (ret);
}
