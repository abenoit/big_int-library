/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_int2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 19:10:59 by abenoit           #+#    #+#             */
/*   Updated: 2020/05/27 17:04:29 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "big_int.h"

uint16_t		sub_8(uint8_t val_1, uint8_t val_2, uint64_t *borrow)
{
	uint16_t		tmp;
	int				i;
	int				res;

	i = -1;
	res = 0;
	while (++i < 8)
	{
		tmp = 0;
		if ((val_1 & (1 << i)) ^ (val_2 & (1 << i)))
		{
			tmp += (1 << i);
			if (val_2 & (1 << i))
				*borrow += (1 << (i + 1));
		}
		if ((tmp & (1 << i)) ^ (*borrow & (1 << i)))
		{
			res += (1 << i);
			if (*borrow & (1 << i))
				*borrow += (1 << (i + 1)) - (1 << i);
		}
		else if ((tmp & (1 << i)) && (*borrow & (1 << i)))
			*borrow -= (1 << i);
	}
	return (res);
}

uint64_t		sub_64(uint64_t val_1, uint64_t val_2, uint64_t *borrow)
{
	uint8_t			*tmp1;
	uint8_t			*tmp2;
	uint64_t		res;
	uint64_t		tmp;
	int				i;

	i = 8;
	res = 0;
	tmp = 0;
	tmp1 = (uint8_t*)&val_1;
	tmp2 = (uint8_t*)&val_2;
	while (i > 0)
	{
		tmp = (uint64_t)sub_8(*tmp1, *tmp2, borrow);
		res += tmp << (8 * (8 - i));
		if (*borrow > 0)
			*borrow = 1;
		i--;
		tmp1++;
		tmp2++;
	}
	return (res);
}

t_big_int		sub_big_int(t_big_int nb1, t_big_int nb2)
{
	uint64_t	borrow;
	int			i;
	t_big_int	result;

	i = 0;
	borrow = 0;
	result.size = nb1.size;
	while (i < result.size)
	{
		result.val[i] = sub_64(nb1.val[i], nb2.val[i], &borrow);
		i++;
	}
	while (result.val[result.size - 1] == 0 && result.size != 1)
		result.size--;
	return (result);
}

t_big_int		diff_big_int(t_big_int nb1, t_big_int nb2)
{
	uint64_t	borrow;
	int			i;
	t_big_int	result;
	t_big_int	biggy;
	t_big_int	small;

	i = 0;
	borrow = 0;
	biggy = ((comp_big_int(nb1, nb2) > 0) ? nb1 : nb2);
	small = ((comp_big_int(nb1, nb2) < 0) ? nb1 : nb2);
	result.size = biggy.size;
	while (i < result.size)
	{
		result.val[i] = sub_64(biggy.val[i], small.val[i], &borrow);
		i++;
	}
	while (result.val[result.size - 1] == 0 && result.size != 1)
		result.size--;
	return (result);
}
